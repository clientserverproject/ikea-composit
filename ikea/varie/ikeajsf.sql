
    create table AbstractElementFurniture (
        id int8 not null,
        code varchar(255),
        date timestamp,
        name varchar(255),
        price float4 not null,
        quantity int4 not null,
        elementComplex_id int8,
        primary key (id)
    );

    create table ElementFurnitureComplex (
        id int8 not null,
        primary key (id)
    );

    create table ElementFurnitureSimple (
        id int8 not null,
        primary key (id)
    );

    create table ElementSingle (
        id int8 not null,
        codice varchar(255),
        costo float4 not null,
        date timestamp,
        nome varchar(255),
        quantity int4 not null,
        elementSingle_id int8,
        primary key (id)
    );

    create table utente (
        id int8 not null,
        attivo boolean not null,
        lastLogin timestamp,
        nome varchar(255),
        nomeUtente varchar(255),
        password varchar(255),
        ruolo varchar(255),
        primary key (id)
    );

    alter table AbstractElementFurniture 
        add constraint UK_dkgotp93ycj3n7wbqdnkf29fu unique (name);

    alter table utente 
        add constraint UK_7hipuu05v6vcqr7wbl8q7p4t2 unique (nomeUtente);

    alter table AbstractElementFurniture 
        add constraint FK_ne3u83aqc9o0h2nln8dg8v25o 
        foreign key (elementComplex_id) 
        references ElementFurnitureComplex;

    alter table ElementFurnitureComplex 
        add constraint FK_87uktm93gsqxctnnwn2g694fo 
        foreign key (id) 
        references AbstractElementFurniture;

    alter table ElementFurnitureSimple 
        add constraint FK_97j4nckw86gy3p468gt78y8jv 
        foreign key (id) 
        references AbstractElementFurniture;

    alter table ElementSingle 
        add constraint FK_75dh90bblfwbhhfihsl5eru9l 
        foreign key (elementSingle_id) 
        references AbstractElementFurniture;

    create table hibernate_sequences (
         sequence_name varchar(255),
         sequence_next_hi_value int4 
    );
