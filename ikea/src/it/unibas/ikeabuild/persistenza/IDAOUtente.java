package it.unibas.ikeabuild.persistenza;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.modello.Utente;

public interface IDAOUtente extends IDAOGenerico<Utente> {
     
    public Utente findByNomeUtente(String nomeUtente) throws DAOException;

    public void save(Utente utente) throws DAOException;

    public void delete(Utente utente) throws DAOException;

}
