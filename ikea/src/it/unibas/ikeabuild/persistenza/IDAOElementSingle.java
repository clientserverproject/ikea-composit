/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.persistenza;

import it.unibas.ikeabuild.modello.composit.ElementSingle;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface IDAOElementSingle extends IDAOGenerico<ElementSingle>{
    
}
