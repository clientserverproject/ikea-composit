/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.persistenza;

import it.unibas.ikeabuild.persistenza.hibernate.model.DAOCabinetHibernate;
import it.unibas.ikeabuild.persistenza.mock.DAOElementSingleMock;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class DAOUtil {
    
    private IDAOCabinet daoCabinet;
    private IDAOElementSingle daoElementSingle;

    public DAOUtil() {
        initBeans();
    }
    
    //TODO Add configuration XML
    public void initBeans(){
        daoCabinet = new DAOCabinetHibernate();
        daoElementSingle = new DAOElementSingleMock();
    }
    
    //Getter and setter
    public IDAOCabinet getDaoCabinet() {
        return daoCabinet;
    }

    public void setDaoCabinet(IDAOCabinet daoCabinet) {
        this.daoCabinet = daoCabinet;
    }

    public IDAOElementSingle getDaoElementSingle() {
        return daoElementSingle;
    }

    public void setDaoElementSingle(IDAOElementSingle daoElementSingle) {
        this.daoElementSingle = daoElementSingle;
    }
}
