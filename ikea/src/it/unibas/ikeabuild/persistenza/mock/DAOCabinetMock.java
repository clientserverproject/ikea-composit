/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.persistenza.mock;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;
import it.unibas.ikeabuild.modello.composit.ElementFurnitureSimple;
import it.unibas.ikeabuild.modello.composit.ElementSingle;
import it.unibas.ikeabuild.persistenza.IDAOCabinet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class DAOCabinetMock implements IDAOCabinet{

    private List<AbstractElementFurniture> cabinet = new ArrayList<>();

    public DAOCabinetMock() {
        
        ElementSingle tableElementOne = new ElementSingle();
        tableElementOne.setCodice("PL-1234");
        tableElementOne.setNome("Piede in legno h.65");
        tableElementOne.setCosto((float) 13.50);
        tableElementOne.setQuantity(4);
        
        ElementSingle tableElementTwoo=  new ElementSingle();
        tableElementTwoo.setCodice("R-66988");
        tableElementTwoo.setNome("Piano	in legno 90x60");
        tableElementTwoo.setCosto((float) 69.50);
        tableElementTwoo.setQuantity(1);
        
        ElementSingle tableElementThree = new ElementSingle();
        tableElementThree.setCodice("98Q99");
        tableElementThree.setNome("Vite	filettata legno	D1");
        tableElementThree.setCosto((float) 0.03);
        tableElementThree.setQuantity(25);
        
        AbstractElementFurniture table = new ElementFurnitureSimple("Tavolo in	legno");
        table.addElementSingle(tableElementOne);
        table.addElementSingle(tableElementTwoo);
        table.addElementSingle(tableElementThree);
        
        cabinet.add(table);
    }
    
    @Override
    public AbstractElementFurniture findById(Long id, boolean lock) throws DAOException {
        return cabinet.get(0);
    }

    @Override
    public List<AbstractElementFurniture> findAll() throws DAOException {
        return cabinet;
    }

    @Override
    public List<AbstractElementFurniture> findAll(int offset, int limite) throws DAOException {
       return cabinet;
    }

    @Override
    public AbstractElementFurniture makePersistent(AbstractElementFurniture entity) throws DAOException {
        cabinet.add(entity);
        return entity;
    }

    @Override
    public void makeTransient(AbstractElementFurniture entity) throws DAOException {
        cabinet.remove(entity);
    }

    @Override
    public void lock(AbstractElementFurniture entity) throws DAOException {
        //doNothing
    }

    @Override
    public AbstractElementFurniture findByName(String name) throws DAOException {
        return cabinet.get(0);
    }
    
}
