/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.persistenza.mock;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.modello.composit.ElementSingle;
import it.unibas.ikeabuild.persistenza.IDAOElementSingle;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class DAOElementSingleMock implements IDAOElementSingle{

    private List<ElementSingle> elements = new ArrayList<>();

    public DAOElementSingleMock() {
        ElementSingle tableElementOne = new ElementSingle();
        tableElementOne.setCodice("PL-1234");
        tableElementOne.setNome("Piede	in legno h.65");
        tableElementOne.setCosto((float) 13.50);
        
        ElementSingle tableElementTwoo=  new ElementSingle();
        tableElementTwoo.setCodice("R-66988");
        tableElementTwoo.setNome("Piano	in legno 90x60");
        tableElementTwoo.setCosto((float) 69.50);
        
        ElementSingle tableElementThree = new ElementSingle();
        tableElementThree.setCodice("98Q99");
        tableElementThree.setNome("Vite	filettata legno	D1");
        tableElementThree.setCosto((float) 0.03);
        
        elements.add(tableElementOne);
        elements.add(tableElementTwoo);
        elements.add(tableElementThree);
    }
    
    @Override
    public ElementSingle findById(Long id, boolean lock) throws DAOException {
        return elements.get(0);
    }

    @Override
    public List<ElementSingle> findAll() throws DAOException {
        return elements;
    }

    @Override
    public List<ElementSingle> findAll(int offset, int limite) throws DAOException {
        return elements;
    }

    @Override
    public ElementSingle makePersistent(ElementSingle entity) throws DAOException {
        elements.add(entity);
        return entity;
    }

    @Override
    public void makeTransient(ElementSingle entity) throws DAOException {
        elements.remove(entity);
    }

    @Override
    public void lock(ElementSingle entity) throws DAOException {
        //do nothing
    }
    
    
    
}
