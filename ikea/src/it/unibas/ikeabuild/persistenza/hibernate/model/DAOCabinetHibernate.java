/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.persistenza.hibernate.model;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;
import it.unibas.ikeabuild.persistenza.IDAOCabinet;
import it.unibas.ikeabuild.persistenza.hibernate.DAOGenericoHibernate;
import java.util.List;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class DAOCabinetHibernate extends DAOGenericoHibernate<AbstractElementFurniture> implements IDAOCabinet{

    public DAOCabinetHibernate() {
        super(AbstractElementFurniture.class);
    }

    @Override
    public AbstractElementFurniture findByName(String name) throws DAOException {
        List<AbstractElementFurniture> elements = super.findByCriteria(Restrictions.eq("name", name));
        if(!elements.isEmpty()){
            return elements.get(0);
        }
        return null;
    }
    
}
