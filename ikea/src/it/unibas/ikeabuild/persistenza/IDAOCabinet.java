/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.persistenza;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface IDAOCabinet extends IDAOGenerico<AbstractElementFurniture>{
    
    AbstractElementFurniture findByName(String name) throws DAOException;
}
