/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.vista;

import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;
import it.unibas.ikeabuild.modello.composit.ElementSingle;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class ViewNewFurniture {

    private String name;
    private String code;
    //Single element choise
    private List<ElementSingle> elmentsSingle = new ArrayList<>();
    private String nameElement;
    private String codeElement;
    private float priceElement;
    private int quantityElement;
    //Single furniture choinse
    private List<AbstractElementFurniture> furnitures = new ArrayList<>();
    private String nameFurniture;
    private String codeFurniture;
    private float priceFurniture;
    private int quantityFurniture;
    
    //getter and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<ElementSingle> getElmentsSingle() {
        return elmentsSingle;
    }

    public void setElmentsSingle(List<ElementSingle> elmentsSingle) {
        this.elmentsSingle = elmentsSingle;
    }

    public List<AbstractElementFurniture> getFurnitures() {
        return furnitures;
    }

    public void setFurnitures(List<AbstractElementFurniture> furnitures) {
        this.furnitures = furnitures;
    }
    
    public boolean isNameNull(){
        return name == null;
    }
    
    public boolean isCodeNull(){
        return code == null;
    }

    public String getNameElement() {
        return nameElement;
    }

    public void setNameElement(String nameElement) {
        this.nameElement = nameElement;
    }

    public String getCodeElement() {
        return codeElement;
    }

    public void setCodeElement(String codeElement) {
        this.codeElement = codeElement;
    }

    public float getPriceElement() {
        return priceElement;
    }

    public void setPriceElement(float priceElement) {
        this.priceElement = priceElement;
    }

    public int getQuantityElement() {
        return quantityElement;
    }

    public void setQuantityElement(int quantityElement) {
        this.quantityElement = quantityElement;
    }

    public String getNameFurniture() {
        return nameFurniture;
    }

    public void setNameFurniture(String nameFurniture) {
        this.nameFurniture = nameFurniture;
    }

    public String getCodeFurniture() {
        return codeFurniture;
    }

    public void setCodeFurniture(String codeFurniture) {
        this.codeFurniture = codeFurniture;
    }

    public float getPriceFurniture() {
        return priceFurniture;
    }

    public void setPriceFurniture(float priceFurniture) {
        this.priceFurniture = priceFurniture;
    }

    public int getQuantityFurniture() {
        return quantityFurniture;
    }

    public void setQuantityFurniture(int quantityFurniture) {
        this.quantityFurniture = quantityFurniture;
    }
    
    public boolean isVisualizableTable(){
        return isCodeNull() && isNameNull();
    }
    

}
