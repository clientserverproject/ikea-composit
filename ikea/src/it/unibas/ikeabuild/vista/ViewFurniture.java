/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.vista;

import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class ViewFurniture {
    
    private List<AbstractElementFurniture> elements = new ArrayList<>();
    private List<AbstractElementFurniture> elementInsideChoise = new ArrayList<>();
    private AbstractElementFurniture elemement; 
    
   
    public boolean isComplex(){
        return !elementInsideChoise.isEmpty();
    }
    
    //getter and setter
    public List<AbstractElementFurniture> getElements() {
        return elements;
    }

    public void setElements(List<AbstractElementFurniture> elements) {
        this.elements = elements;
    }

    public AbstractElementFurniture getElemement() {
        return elemement;
    }

    public void setElemement(AbstractElementFurniture elemement) {
        this.elemement = elemement;
    }

    public List<AbstractElementFurniture> getElementInsideChoise() {
        return elementInsideChoise;
    }

    public void setElementInsideChoise(List<AbstractElementFurniture> elementInsideChoise) {
        this.elementInsideChoise = elementInsideChoise;
    }
    
}
