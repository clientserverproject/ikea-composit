/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.controllo.actions;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;
import it.unibas.ikeabuild.modello.composit.ElementFurnitureComplex;
import it.unibas.ikeabuild.persistenza.DAOUtil;
import it.unibas.ikeabuild.vista.ViewFurniture;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class ActionFourniture {
    
    private final transient Log LOGGER = LogFactory.getLog(getClass().getSimpleName());
    
    @ManagedProperty(value = "#{dAOUtil}")
    private DAOUtil daoutil;
    @ManagedProperty(value = "#{viewFurniture}")
    private ViewFurniture view;
    
    public void initBeans() {
        try {
            List<AbstractElementFurniture> elements = daoutil.getDaoCabinet().findAll();
            view.setElements(elements);
        } catch (DAOException ex) {
            LOGGER.error("Exception generated: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    public void viewAllElementForFurniture(AbstractElementFurniture elementChoise){
        try {
            if(elementChoise == null){
                throw new IllegalArgumentException("Element chois null");
            }
            
            daoutil.getDaoCabinet().makePersistent(elementChoise);
            
            
            view.setElemement(elementChoise);
            if(elementChoise.isComplex()){
                LOGGER.debug("Is an element complex");
                ElementFurnitureComplex complex = (ElementFurnitureComplex) elementChoise;
                view.setElementInsideChoise(complex.getCabinets());
            }
            LOGGER.debug("********** ADDED element choise to View ************");
        } catch (DAOException ex) {
            LOGGER.error("Exception Generated is: " + ex.getLocalizedMessage());
            ex.printStackTrace();
        }
    }
    
    //getter and setter
    public DAOUtil getDaoutil() {
        return daoutil;
    }

    public void setDaoutil(DAOUtil daoutil) {
        this.daoutil = daoutil;
    }

    public ViewFurniture getView() {
        return view;
    }

    public void setView(ViewFurniture view) {
        this.view = view;
    }
    
    
}
