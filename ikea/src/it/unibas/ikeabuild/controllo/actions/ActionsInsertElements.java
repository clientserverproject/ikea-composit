/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.controllo.actions;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.eccezioni.InitBeansException;
import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;
import it.unibas.ikeabuild.modello.composit.ElementFurnitureComplex;
import it.unibas.ikeabuild.modello.composit.ElementFurnitureSimple;
import it.unibas.ikeabuild.modello.composit.ElementSingle;
import it.unibas.ikeabuild.modello.strategy.IPriceStrategy;
import it.unibas.ikeabuild.modello.strategy.PriceStrategy;
import it.unibas.ikeabuild.modello.visitor.ElementFurnitureVisitor;
import it.unibas.ikeabuild.modello.visitor.IElementVisitor;
import it.unibas.ikeabuild.persistenza.DAOUtil;
import it.unibas.ikeabuild.vista.ViewNewFurniture;
import java.util.GregorianCalendar;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@ManagedBean
@ViewScoped
public class ActionsInsertElements {

    private final transient Log LOGGER = LogFactory.getLog(getClass().getSimpleName());

    @ManagedProperty(value = "#{dAOUtil}")
    private DAOUtil daoUtil;
    @ManagedProperty(value = "#{viewNewFurniture}")
    private ViewNewFurniture view;

    public void initBeans() {
        //for moment do nothing
    }

    public void insertNewElementSingle() {
        if (view == null) {
            throw new InitBeansException("View not inizializate");
        }

        String nameElement = view.getNameElement();
        String codeElement = view.getCodeElement();
        float priceElement = view.getPriceElement();
        int quantityElement = view.getQuantityElement();

        ElementSingle newElement = new ElementSingle();
        newElement.setCodice(codeElement);
        newElement.setNome(nameElement);
        newElement.setCosto(priceElement);
        newElement.setDate(new GregorianCalendar());
        newElement.setQuantity(quantityElement);

        view.getElmentsSingle().add(newElement);
        LOGGER.debug("********** Element with name " + newElement.getNome() + " add to list **************");
    }

    public void insertNewFurniture() {
        if (view == null) {
            throw new InitBeansException("View not inizializate");
        }

        String nameFurniture = view.getNameFurniture();
        String codeFurniture = view.getCodeFurniture();
        float priceFurniture = view.getPriceFurniture();
        int quantityFurniture = view.getQuantityFurniture();

        ElementFurnitureSimple newFurniture = new ElementFurnitureSimple();
        newFurniture.setCode(codeFurniture);
        newFurniture.setName(nameFurniture);
        newFurniture.setDate(new GregorianCalendar());
        newFurniture.setPrice(priceFurniture);
        newFurniture.setQuantity(quantityFurniture);

        view.getFurnitures().add(newFurniture);
        LOGGER.debug("********** Furniture with name " + newFurniture.getName() + " add to list **************");
    }

    //TODO do refactoring
    //TODO bug not load apply correctly strategy for calculate the price
    public void createNewFurniture() {
        if (view == null) {
            throw new InitBeansException("View not inizializate");
        }

        if (view.getFurnitures().isEmpty()) {
            try {
                //Furniture simple
                LOGGER.debug("Adde new simple element with name: " + view.getName());
                AbstractElementFurniture element = daoUtil.getDaoCabinet().findByName(view.getName());
                if(element != null){
                    FacesContext.getCurrentInstance().addMessage(":growl",
                        new FacesMessage(
                                FacesMessage.SEVERITY_WARN,
                                "Info",
                                "Mobile gia' contenuto"));
                     return;
                }
                ElementFurnitureSimple newFurniture = new ElementFurnitureSimple(view.getName());
                newFurniture.setCode(view.getCode());

                newFurniture.setDate(new GregorianCalendar());
                newFurniture.setQuantity(1);
                
                for (ElementSingle elementSingle : view.getElmentsSingle()) {
                    LOGGER.debug("I'm adding " + elementSingle.getNome() + " element single to new element");
                    newFurniture.addElementSingle(elementSingle);
                }
                //TODO calculate pricP
                IElementVisitor visitor = new ElementFurnitureVisitor();
                IPriceStrategy strategy = new PriceStrategy();
                strategy.setVisitor(visitor);
                
                float price = strategy.getFinalPrice(newFurniture);
                LOGGER.debug("********** Price calculate: " + price + " ***********");
                newFurniture.setPrice(price);
                
                daoUtil.getDaoCabinet().makePersistent(newFurniture);
                
                FacesContext.getCurrentInstance().addMessage(":growl",
                new FacesMessage(
                        FacesMessage.SEVERITY_INFO,
                        "Conferma aggiunta",
                        "Creazione mobile con successo\n Prezzo: " + newFurniture.getPrice()));
            } catch (DAOException ex) {
                LOGGER.error("******* Exeption generated is: " + ex.getLocalizedMessage() + " **********");
                FacesContext.getCurrentInstance().addMessage(":growl",
                        new FacesMessage(
                                FacesMessage.SEVERITY_FATAL,
                                "ERRORE BD",
                                "Riprova piu tardi, si e' verificato un errore con il database"));
                ex.printStackTrace();
            }
        } else {
            try {
                LOGGER.debug("Added new Complex element with name: " + view.getName());
                AbstractElementFurniture elementTmp = daoUtil.getDaoCabinet().findByName(view.getName());
                if(elementTmp != null){
                    FacesContext.getCurrentInstance().addMessage(":growl",
                        new FacesMessage(
                                FacesMessage.SEVERITY_WARN,
                                "Info",
                                "Mobile gia' contenuto"));
                    return;
                }
                ElementFurnitureComplex newFurniture = new ElementFurnitureComplex(view.getName());
                newFurniture.setCode(view.getCode());
                
                newFurniture.setDate(new GregorianCalendar());
                newFurniture.setQuantity(1);
                for (ElementSingle elementSingle : view.getElmentsSingle()) {
                    LOGGER.debug("I'm adding " + elementSingle.getNome() + " element single to new element");
                    newFurniture.addElementSingle(elementSingle);
                }
                
                for(AbstractElementFurniture element : view.getFurnitures()){
                    LOGGER.debug("I'm adding element complex to new element");
                    newFurniture.addCabinet(element);
                }
                
                IElementVisitor visitor = new ElementFurnitureVisitor();
                
                IPriceStrategy strategy = new PriceStrategy();
                strategy.setVisitor(visitor);
                
                float price = strategy.getFinalPrice(newFurniture);
                newFurniture.setPrice(price);
                
                daoUtil.getDaoCabinet().makePersistent(newFurniture);
                
               FacesContext.getCurrentInstance().addMessage(":growl",
                new FacesMessage(
                        FacesMessage.SEVERITY_INFO,
                        "Conferma aggiunta",
                        "Creazione mobile con successo\n Prezzo: " + newFurniture.getPrice()));
            } catch (DAOException ex) {
                LOGGER.error("******* Exeption generated is: " + ex.getLocalizedMessage() + " **********");
                FacesContext.getCurrentInstance().addMessage(":growl",
                        new FacesMessage(
                                FacesMessage.SEVERITY_FATAL,
                                "ERRORE BD",
                                "Riprova piu tardi, si e' verificato un errore con il database"));
                ex.printStackTrace();
            }
            
        }

    }

    //getter and setter
    public DAOUtil getDaoUtil() {
        return daoUtil;
    }

    public void setDaoUtil(DAOUtil daoUtil) {
        this.daoUtil = daoUtil;
    }

    public ViewNewFurniture getView() {
        return view;
    }

    public void setView(ViewNewFurniture view) {
        this.view = view;
    }

}
