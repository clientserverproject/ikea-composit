/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.visitor;

import it.unibas.ikeabuild.modello.composit.ElementFurnitureComplex;
import it.unibas.ikeabuild.modello.composit.ElementFurnitureSimple;
import it.unibas.ikeabuild.modello.strategy.chain.ContextPrice;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface IElementVisitor {
    
    void visitElementComplex(ElementFurnitureComplex element);
    void visitElementSimple(ElementFurnitureSimple element);
    
    //Getter
    ContextPrice getContext();
}
