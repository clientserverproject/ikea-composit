/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.visitor;

import it.unibas.ikeabuild.modello.composit.ElementFurnitureComplex;
import it.unibas.ikeabuild.modello.composit.ElementFurnitureSimple;
import it.unibas.ikeabuild.modello.composit.ElementSingle;
import it.unibas.ikeabuild.modello.composit.IElementFurniture;
import it.unibas.ikeabuild.modello.strategy.chain.ContextPrice;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class ElementFurnitureVisitor implements IElementVisitor{
    
    private int numbarElementComplex;
    private float sumCostSingleElement;
    private ContextPrice context;

    @Override
    public void visitElementComplex(ElementFurnitureComplex element) {
        if(element == null){
            throw new IllegalArgumentException("Element null");
        }
        numbarElementComplex++;
        for(IElementFurniture elementFurniture :  element.getCabinets()){
            sumCostSingleElement += element.getPrice();
            elementFurniture.accept(this);
        }
        
        //Visit all element simple
        for(ElementSingle elementSingle : element.getElements()){
            sumCostSingleElement += elementSingle.getCosto();
        }
    }

    @Override
    public void visitElementSimple(ElementFurnitureSimple element) {
        if(element == null){
            throw new IllegalArgumentException("Element null");
        }
        for(ElementSingle elementSingle : element.getElements()){
            sumCostSingleElement += elementSingle.getCosto() * elementSingle.getQuantity();
        }
    }
    
    //Getter propriety
    public ContextPrice getContext() {
        context = new ContextPrice();
        context.setCostElementSingle(sumCostSingleElement);
        context.setElementComplex(numbarElementComplex);
        return context;
    }
   
}
