/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.composit;

import it.unibas.ikeabuild.modello.visitor.IElementVisitor;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@Entity
public class ElementFurnitureComplex extends AbstractElementFurniture{
    
    private List<AbstractElementFurniture> cabinets = new ArrayList<>();

    public ElementFurnitureComplex(String nome) {
        super(nome);
    }

    public ElementFurnitureComplex() {}
    

    @OneToMany(mappedBy = "elementComplex", orphanRemoval = true, cascade = {CascadeType.ALL})
    public List<AbstractElementFurniture> getCabinets() {
        return cabinets;
    }

    public void setCabinets(List<AbstractElementFurniture> cabinets) {
        this.cabinets = cabinets;
    }
    
    public void addCabinet(AbstractElementFurniture element){
        super.addCabinet(element);
        cabinets.add(element);
        element.setElementComplex(this);
    }

    
    @Override
    public void accept(IElementVisitor visitor) {
        super.accept(visitor);
        visitor.visitElementComplex(this);
    }

    @Override
    @Transient
    public int getNumbarComponent() {
        return cabinets.size();
    }

    @Override
    @Transient
    public boolean isComplex() {
        return true;
    }
    
}
