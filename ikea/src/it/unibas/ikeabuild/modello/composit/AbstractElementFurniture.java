/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.composit;

import it.unibas.ikeabuild.modello.visitor.IElementVisitor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractElementFurniture implements IElementFurniture {

    private Long id;
    private String name;
    private String code;
    private Calendar date;
    private int quantity;
    private float price;
    private List<ElementSingle> elements = new ArrayList<>();
    private ElementFurnitureComplex elementComplex;

    public AbstractElementFurniture(String nome) {
        this.name = nome;
    }

    public AbstractElementFurniture() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(unique = true)
    public String getName() {
        return name;
    }

    public void setName(String nome) {
        this.name = nome;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    @OneToMany(mappedBy = "elementSingle", orphanRemoval = true, cascade = {CascadeType.ALL})
    public List<ElementSingle> getElements() {
        return elements;
    }

    public void setElements(List<ElementSingle> elements) {
        this.elements = elements;
    }

    @Override
    public void addElementSingle(ElementSingle element) {
        if (element == null) {
            throw new IllegalArgumentException("element null, I can not add to element into Element simple");
        }
        elements.add(element);
        element.setElementSingle(this);
    }
    
    @ManyToOne
    public ElementFurnitureComplex getElementComplex() {
        return elementComplex;
    }

    public void setElementComplex(ElementFurnitureComplex elementComplex) {
        this.elementComplex = elementComplex;
    }
    
    @Transient
    @Override
    public int getNumbarComponent() {
        return elements.size();
    }

    @Override
    public void addCabinet(AbstractElementFurniture element) {
        if (element == null) {
            throw new IllegalArgumentException("Element null, I can't add the element to the cabinets");
        }
    }

    @Override
    public void accept(IElementVisitor visitor) {
        if (visitor == null) {
            throw new IllegalArgumentException("Visitor null");
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
