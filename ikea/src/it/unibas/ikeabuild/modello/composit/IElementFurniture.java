/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.composit;

import it.unibas.ikeabuild.modello.visitor.IElementVisitor;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface IElementFurniture {
    
    String getName();
    
    int getNumbarComponent();
    
    void accept(IElementVisitor visitor);
    
    void addElementSingle(ElementSingle element);
    
    void addCabinet(AbstractElementFurniture element);
    
    boolean isComplex();
}
