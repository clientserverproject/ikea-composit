/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.composit;

import it.unibas.ikeabuild.modello.visitor.IElementVisitor;
import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
@Entity
public class ElementFurnitureSimple extends AbstractElementFurniture{


    public ElementFurnitureSimple(String nome) {
        super(nome);
    }

    public ElementFurnitureSimple() {}

    @Override
    public void addElementSingle(ElementSingle element) {
        super.addElementSingle(element); 
    }
    
    @Override
    @Transient
    public boolean isComplex() {
        return false;
    }
    
    @Override
    public void accept(IElementVisitor visitor) {
        super.accept(visitor);
        visitor.visitElementSimple(this);
    }
    
}
