/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.strategy;

import it.unibas.ikeabuild.modello.composit.IElementFurniture;
import it.unibas.ikeabuild.modello.strategy.chain.ContextPrice;
import it.unibas.ikeabuild.modello.visitor.IElementVisitor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import it.unibas.ikeabuild.modello.strategy.chain.IApplyPriceHeadler;
import it.unibas.ikeabuild.modello.strategy.chain.MaximumSixElementComplex;
import it.unibas.ikeabuild.modello.strategy.chain.MoreSixElementComplex;
import it.unibas.ikeabuild.modello.strategy.chain.OnlyOneElementComplex;
import it.unibas.ikeabuild.modello.strategy.chain.OnlySimpleElement;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class PriceStrategy implements IPriceStrategy {

    private final Log LOGGER = LogFactory.getLog(getClass().getCanonicalName());

    private IElementVisitor visitor;
    
    private List<IApplyPriceHeadler> combination = new ArrayList<>();

    public PriceStrategy() {
        combination.add(new OnlySimpleElement());
        combination.add(new OnlyOneElementComplex());
        combination.add(new MaximumSixElementComplex());
        combination.add(new MoreSixElementComplex());
    }
    
    public IElementVisitor getVisitor() {
        return visitor;
    }

    public void setVisitor(IElementVisitor visitor) {
        this.visitor = visitor;
    }
    
    @Override
    public float getFinalPrice(IElementFurniture element) {
        if (visitor == null || element == null) {
            if (visitor == null) {
                LOGGER.error("Visitor is null, I can't apply this strategy correctly");
                throw new IllegalArgumentException("Visitor is null, I can't apply this strategy correctly");
            }
            LOGGER.error("Element is null, I can't apply this strategy correctly");
            throw new IllegalArgumentException("Element is null, I can't apply this strategy correctly");

        }
        element.accept(visitor);

        ContextPrice context = visitor.getContext();
        
        Iterator iterator = combination.iterator();
        boolean flag = false;
        
        while(iterator.hasNext() && !flag){
            LOGGER.debug("Analizating iterator with description: " + iterator.toString());
            IApplyPriceHeadler headler = (IApplyPriceHeadler) iterator.next();
            flag = headler.doHeadler(context);
            if(flag) LOGGER.debug("Final price calculate: " + context.getFinalPrice());
        }
        
        return context.getFinalPrice();
     }

}
