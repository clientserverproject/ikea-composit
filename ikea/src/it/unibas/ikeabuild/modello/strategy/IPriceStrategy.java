/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.strategy;

import it.unibas.ikeabuild.modello.composit.IElementFurniture;
import it.unibas.ikeabuild.modello.visitor.IElementVisitor;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface IPriceStrategy {
    
    
    float getFinalPrice(IElementFurniture element);
    
    void setVisitor(IElementVisitor visitor);
    
}
