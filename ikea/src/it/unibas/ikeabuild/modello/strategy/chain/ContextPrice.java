/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.strategy.chain;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class ContextPrice {
    
    private int elementComplex;
    private float costElementSingle;
    private float finalPrice;

    public int getElementComplex() {
        return elementComplex;
    }

    public void setElementComplex(int elementComplex) {
        this.elementComplex = elementComplex;
    }

    public float getCostElementSingle() {
        return costElementSingle;
    }

    public void setCostElementSingle(float costElementSingle) {
        this.costElementSingle = costElementSingle;
    }

    public float getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(float finalPrice) {
        this.finalPrice = finalPrice;
    }
    
}
