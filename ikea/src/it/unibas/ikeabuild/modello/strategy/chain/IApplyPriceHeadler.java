/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.strategy.chain;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public interface IApplyPriceHeadler {
    
    
    boolean doHeadler(ContextPrice context);
    
    float addPercentual(float priceStart, int percentual);
    
}
