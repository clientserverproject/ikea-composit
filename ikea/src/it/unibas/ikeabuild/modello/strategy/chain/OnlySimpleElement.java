/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.strategy.chain;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class OnlySimpleElement extends AbstractApplyPrinceHeadler{

    private final Log LOGGER = LogFactory.getLog(this.getClass().getSimpleName());
    
    @Override
    public boolean doHeadler(ContextPrice context) {
       super.doHeadler(context);
        
        if(context.getElementComplex() == 0){
         float finalPrice = context.getCostElementSingle() + addPercentual(context.getCostElementSingle(), 15);
            LOGGER.debug("Percentual calculate is: " + finalPrice);
            context.setFinalPrice(finalPrice);
            return true;
        }
        return false;
    }
    
}
