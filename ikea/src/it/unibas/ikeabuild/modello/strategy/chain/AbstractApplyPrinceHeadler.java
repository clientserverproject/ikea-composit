/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.ikeabuild.modello.strategy.chain;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public abstract class AbstractApplyPrinceHeadler implements IApplyPriceHeadler{
    
    private final Log LOGGER = LogFactory.getLog(this.getClass().getSimpleName());

    @Override
    public boolean doHeadler(ContextPrice context) {
        if (context == null) {
            LOGGER.debug("Contex null");
            throw new IllegalArgumentException("Context null, I con't apply this headler for calculating final price");
        }
        
        return false;
    }
    
     @Override
    public float addPercentual(float priceStart, int percentual) {
        if (percentual < 0) {
            throw new IllegalArgumentException("Not exist the percentual negative");
        }
        
        float percentualPrice = ((float)percentual / 100);

        LOGGER.debug("Percentual calculating is: " + percentualPrice);
        return percentualPrice * priceStart;
    }
    
}
