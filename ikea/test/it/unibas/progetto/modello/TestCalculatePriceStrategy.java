/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.progetto.modello;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;
import it.unibas.ikeabuild.modello.composit.ElementFurnitureComplex;
import it.unibas.ikeabuild.modello.strategy.IPriceStrategy;
import it.unibas.ikeabuild.modello.strategy.PriceStrategy;
import it.unibas.ikeabuild.modello.visitor.ElementFurnitureVisitor;
import it.unibas.ikeabuild.modello.visitor.IElementVisitor;
import it.unibas.ikeabuild.persistenza.mock.DAOCabinetMock;
import it.unibas.ikeabuild.persistenza.mock.DAOElementSingleMock;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class TestCalculatePriceStrategy {
    
    private IPriceStrategy strategy;
    private DAOCabinetMock daoCabinet;
    private DAOElementSingleMock daoElementSingle;
    private IElementVisitor visitor;
    
    @Before
    public void initBeans(){
        visitor = new ElementFurnitureVisitor();
        strategy = new PriceStrategy();
        daoCabinet = new DAOCabinetMock();
        daoElementSingle = new DAOElementSingleMock();
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testStrategyWithVisitorNull() throws DAOException{
        strategy.setVisitor(null);
        
        strategy.getFinalPrice(daoCabinet.findById(null, true));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testStrategyWithElementNull(){
        strategy.setVisitor(visitor);
        
        strategy.getFinalPrice(null);
    }
    
    @Test
    public void testStrategyCalculatePriceOne() throws DAOException{
        strategy.setVisitor(visitor);
        
        float finalPrice = strategy.getFinalPrice(daoCabinet.findById(null, false));
        
        TestCase.assertEquals((float)142.8875, finalPrice);
    }
    
    @Test
    public void testStrategyCalculatePriceTwo() throws DAOException{
        AbstractElementFurniture elementOne = daoCabinet.findById(null, true);
        ElementFurnitureComplex elementTwo = new ElementFurnitureComplex("Douplicate");
        
        elementTwo.addCabinet(elementOne);
        elementTwo.getElements().addAll(daoElementSingle.findAll());

        strategy.setVisitor(visitor);
        
        float finalPrice = strategy.getFinalPrice(elementTwo);
        
        TestCase.assertEquals((float)248.736, finalPrice);
    }
    
    @Test
    public void testStrategyCalculatePriceThree() throws DAOException{
        AbstractElementFurniture elementOne = daoCabinet.findById(null, true);
        ElementFurnitureComplex elementTwo = new ElementFurnitureComplex("Douplicate");
        
        elementTwo.addCabinet(elementOne);
        elementTwo.addCabinet(elementOne);
        elementTwo.getElements().addAll(daoElementSingle.findAll());

        strategy.setVisitor(visitor);
        
        float finalPrice = strategy.getFinalPrice(elementTwo);
        
        TestCase.assertEquals((float)397.836, finalPrice);
    }
    
}
