/*
 * This code is under license Creative Commons Attribution-ShareAlike 1.0
 * <a href="https://creativecommons.org/licenses/by-sa/1.0/legalcode"></a>
 */
package it.unibas.progetto.modello;

import it.unibas.ikeabuild.eccezioni.DAOException;
import it.unibas.ikeabuild.modello.composit.AbstractElementFurniture;
import it.unibas.ikeabuild.modello.composit.ElementFurnitureComplex;
import it.unibas.ikeabuild.modello.composit.ElementSingle;
import it.unibas.ikeabuild.modello.visitor.ElementFurnitureVisitor;
import it.unibas.ikeabuild.modello.visitor.IElementVisitor;
import it.unibas.ikeabuild.persistenza.mock.DAOCabinetMock;
import it.unibas.ikeabuild.persistenza.mock.DAOElementSingleMock;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author https://github.com/vincenzopalazzo
 */
public class TestElementFurnitureVisitor {
    
    private DAOCabinetMock daoCabinet;
    private DAOElementSingleMock daoElementSingle;
    private IElementVisitor visitor;
    
    @Before
    public void initBeans(){
        daoCabinet = new DAOCabinetMock();
        daoElementSingle = new DAOElementSingleMock();
        visitor = new ElementFurnitureVisitor();
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testVisitorElementNull() throws DAOException{
        AbstractElementFurniture element = daoCabinet.findAll().get(0);
        
        element.accept(null);
    }
    
    @Test
    public void testVisitorElementOne() throws DAOException{
        AbstractElementFurniture element = daoCabinet.findAll().get(0);
        
        element.accept(visitor);
        
        TestCase.assertEquals(0, visitor.getContext().getElementComplex());
        TestCase.assertEquals((float)124.25, visitor.getContext().getCostElementSingle());
    }
    
    @Test
    public void testVisitorElementTwo() throws DAOException{
        AbstractElementFurniture elementOne = daoCabinet.findAll().get(0);
        AbstractElementFurniture elementTwo = new ElementFurnitureComplex("CompositElementSame");
        elementTwo.addCabinet(elementOne);
        //The findById mock returned the first element of the list the agument is ingored
        ElementSingle elementSingle = daoElementSingle.findById(null, true);
        float priceNewElementSingle = elementSingle.getCosto();
        
        elementTwo.addElementSingle(elementSingle);
        
        elementTwo.accept(visitor);
        TestCase.assertEquals(1, visitor.getContext().getElementComplex());
        TestCase.assertEquals((float)124.25 + priceNewElementSingle, visitor.getContext().getCostElementSingle());
    }
}
